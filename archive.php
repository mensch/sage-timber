<?php
  $templates = array( 'archive.twig', 'index.twig' );

  $context = Timber::get_context();
  $context['posts'] = Timber::get_posts();

  Timber::render( $templates, $context );