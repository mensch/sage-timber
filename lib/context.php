<?php
  use Roots\Sage\Assets;

  if (!function_exists('add_to_timber_context')) {
    function add_to_timber_context($data){
      $data['primary_menu'] = new TimberMenu('primary_navigation');
      return $data;
    }
    add_filter('timber_context', 'add_to_timber_context');
  }
