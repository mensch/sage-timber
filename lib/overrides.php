<?php
  if (!function_exists('sage_timber_login_logo')) {
    function sage_timber_login_logo() { ?>
        <style type="text/css">
          body.login div#login h1 a {
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icons/sagetimber.svg');
          }
        </style>
    <?php }
    add_action( 'login_enqueue_scripts', 'sage_timber_login_logo' );
  }

  // Wrap embedes in a div container
  if (!function_exists('wrap_oembeds')) {
    function wrap_oembeds($html, $url, $attr, $post_ID) {
      $return = '<div class="entry-content-asset">'.$html.'</div>';
      return $return;
    }
    add_filter( 'embed_oembed_html', 'wrap_oembeds', 10, 4 ) ;
  }
